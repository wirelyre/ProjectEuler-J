problem_0004 =: '>./   (#~ pal)   ,/ *"0/~   100}.i.1000'
problem_0004 =: '>./   (*pal)   ,/ */~   100}.i.1000'
NB.                                      ^~~~~~~~~~~~ numbers 100-999
NB.                             ^~~~~~ multiply all pairs, make list
NB.                    ^~~~~~ turn non-palindromes into 0
NB.              ^~~ maximum
NB. maximum of palindromes in list of product of all pairs of numbers 100-999
