NB. check \, aka "prefix" — might simplify a lot
NB. http://www.jsoftware.com/help/dictionary/d430.htm
asset_0008 =: 'str }:"1 ] 20 51 $ 1!:1 < ''Assets/0008.txt'''
problem_0008 =: '>./ */"1 ] 13{."1 (i.988 1) |. ,/ ". asset_0008'