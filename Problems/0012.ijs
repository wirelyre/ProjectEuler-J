problem_0012 =: '-:(*>:)>: x: 500 i.&1@:< fac"0 -:(*>:) 1+i.100000'
problem_0012 =: '-:(*>:)>: x: (500&<i.1:) fac"0 -:(*>:) 1+i.100000'
NB.                                                     ^~~~~~~~~~ search space
NB.                                             ^~~~~~~ n*(n+1)/2
NB.                                       ^~~~~ number of factors
NB.                           ^~~~~~~~~~~ first index where greater than 500
NB.                        ^~ extend precision for exact answer
NB.              ^~~~~~~~~ convert index into triangle number
NB. triangle number of extended precision of first index where 500 less than
NB. number of factors of triangle number of index less than 100000
