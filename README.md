# ProjectEuler-J

Solutions to [Project Euler problems](https://projecteuler.net/archives) in the
[J programming language](http://www.jsoftware.com/).

## About

The goal of this project is to have a one-line solution for each Project Euler
problem. This is possible in such a terse language as J.

To see the answers to problems 8 and 13, run

```
$ j run.ijs 8 13
   8: 23514624000
  13: 5537376230
```

To see the answers to all problems, run

```
$ j run.ijs all
   1: 233168
   2: 4613732
...
```

* Source solutions are located in `Problems/[problem].ijs`.
* Common functions (*e.g.*, Fibonacci numbers) are specified in `common.ijs`.
* Plain text specific to a particular problem (*e.g.*, a large table) is located
    in `Assets/[problem].ijs`.

## Why?

For one, this is cool. I would love to be able to print out the solutions to
every Project Euler problem on a few sheets of paper.

More practically, J (and APL, and other such languages) have different
information density, compared to more common languages. Whereas a C solution to
one of these problems might be three or four functions long, even a complicated
J solution likely fits on a single line. Any supporting functions, like "sum of
divisors", also fit on a single line. Being able to visually scan, without
scrolling, through the entire source code of a particular problem, is an unusual
boon when working with discrete problems like these.

J also has primitive mathematical functions that are very useful for many
Project Euler problems. For example, producing the `n`th prime number is written
`p: n`; and calculating the `n`th permutation of `x` is written `n A. x`.
Although the mnemonics can be hard to get used to, the functions combine well in
practice.
