run =: 3 : 0
  digits =. -&48 a.i. y
  if. y=&<'all' do.
    dir =. 1!:0 'Problems/*.ijs'
    files =. {."1 dir
    problems =. (4&{.) each files
    run each problems
  elseif. *./ (0<:digits) *. (digits<:9) do.
    problem 10 #. digits
  elseif. do.
    echo 'Skipping invalid problem "',y,'"'
  end.
)

problem =: 3 : 0
  plain =.        '' 8!:0 &.< y
  formatted =.  '4.' 8!:0 &.< y
  padded =. 'r<0>4.' 8!:0 &.< y

  file =. 'Problems/',padded,'.ijs'
  if. # 1!:0 file do.
    stdout formatted,': '
    0!:0 <file
    echo ".". 'problem_',padded
  else.
    echo 'No solution for problem ',plain
  end.
)

0!:0 <'common.ijs'
run each 2}.ARGV

exit 0
